import { Component } from '@angular/core';

import { markdown } from 'markdown';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'markdown-editor';

  input: string = '*markdown*';

  convertedToHTML: string = '';

  constructor() {
    console.log(markdown.toHTML("Hello **World**!"));
  }

  ngOnInit() {
    console.log(markdown.toHTML("Hello *World*!"));
  }

  ngAfterViewInit() {
    //console.log(markdown.toHTML("Hello *World*!"));
    this.convertedToHTML = markdown.toHTML(this.input);
  }

  transformToHTML() {
    this.convertedToHTML = markdown.toHTML(this.input);
  }

}
